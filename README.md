# fast-log

# 介绍
fast-log 又名日志快排，为了方便研发人员对日志的排查

支持 logback、和log4j2日志收集，对业务零侵入。

支持日志链路搜索

es 需要 7.2.0 及以上版本


# 软件架构
软件架构说明
![软件架构设计](https://images.gitee.com/uploads/images/2021/0830/210305_d1aeefe8_695155.png "屏幕截图.png")



# 安装教程

```

<dependency>
                <groupId>io.gitee.xinsuinian</groupId>
                <artifactId>fast-log-spring-boot-start</artifactId>
                <version>${fast.log.version}</version>
            </dependency>
```


# 使用说明

## logback 使用
1、依赖

```
<dependency>
                <groupId>io.gitee.xinsuinian</groupId>
                <artifactId>fast-log-spring-boot-start</artifactId>
                <version>${fast.log.version}</version>
            </dependency>
```

2、使用方式

 **如果使用rocketmq收集日志，logback配置文件如下** 

```
<appender name="rocketMqAppender" class="com.lwq.fast.log.fastlogclient.logback.appender.RocketMqAppender">
		<appName>payment</appName>
		<env>dev</env>
		<namesrvAddr>localhost:9876</namesrvAddr>
	</appender>

<root level="INFO">
		<appender-ref ref="rocketMqAppender" />
	</root>

```


 **如果使用rabbitmq收集日志。logback配置文件中加入如下** 

```
<appender name="rabbitLogAppender" class="com.lwq.fast.log.fastlogclient.logback.appender.RabbitMqAppender">
		<appName>payment</appName>
		<env>dev</env>
		<host>localhost</host>
		<port>5672</port>
		<virtualHost>/</virtualHost>
		<username>guest</username>
		<password>guest</password>
	</appender>


<root level="INFO">
		<appender-ref ref="rabbitLogAppender" />
	</root>
```


 **如果使用redis收集日志。logback配置中加入如下配置** 
```
<appender name="redisLogAppender" class="com.lwq.fast.log.fastlogclient.logback.appender.RedisAppender">
		<appName>payment</appName>
		<env>dev</env>
		<host>localhost</host>
		<port>6379</port>
		<auth></auth>
	</appender>

<root level="INFO">
		
		<appender-ref ref="redisLogAppender" />
		
	</root>
```


## log4j2 使用
1、依赖
```
<dependency>
            <groupId>io.gitee.xinsuinian</groupId>
            <artifactId>fast-log-spring-boot-start</artifactId>
            <<exclusions>
                <exclusion>
                    <groupId>ch.qos.logback</groupId>
                    <artifactId>logback-core</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>ch.qos.logback</groupId>
                    <artifactId>logback-classic</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
```

2、使用方式

 **如果使用rocketmq收集日志，log4j2的配置如下** 
```
<appenders>
<Log4jRocketMqAppender name="RocketmqMqAppender" appName="payment" env="dev" namesrvAddr="localhost:9876" >
            <PatternLayout pattern="${LOG_PATTERN}"/>
        </Log4jRocketMqAppender>

   <root level="info">
            <appender-ref ref="RocketmqMqAppender"/>
        </root>

```




 **如果使用 rabbitmq收集日志，log4j2 的配置如下** 
```
<appenders>
<Log4jRabbitMqAppender name="RabbitMqAppender" appName="payment" env="dev" host="localhost" port="5672" virtualHost="/" username="guest" password="guest">
            <PatternLayout pattern="${LOG_PATTERN}"/>
        </Log4jRabbitMqAppender>
</appenders>

<root level="info">
            <appender-ref ref="RabbitMqAppender"/>
        </root>

```
 **如果使用redis收集日志，log4j2的配置如下** 
```
<appenders>
<Log4jRedisAppender name="Log4jRedisAppender" appName="payment" env="dev" host="localhost" port="6379" auth="">
            <PatternLayout pattern="${LOG_PATTERN}"/>
        </Log4jRedisAppender>
</appenders>
<root level="info">
<appender-ref ref="Log4jRedisAppender"/>
        </root>
```

## 日志链路搜索

### 正常请求使用
引入上述依赖后，启动类加上扫描包，防止基础包不一致，导致失效
```
@SpringBootApplication(scanBasePackages = {"com.lwq.*"})
```
正常请求，加入上边包扫描后，就会生成生个请求的日志链路信息，同时支持feign 调用日志链路

### 定时任务使用
如果在定时任务中也想使用日志链路，可以在对应的定时任务方法上加 ``` @EnableTrace ``` 注解

### RestTemplate 使用
当使用RestTemplate 请求，想使用 日志链路时，需要加上一个过滤器 ``` RestTemplateHeaderInterceptor ```
![输入图片说明](https://images.gitee.com/uploads/images/2021/1109/212042_b192a777_695155.png "QQ截图20211109212053.png")

## server端使用
server 端的配置如下

```
server.port=8081

# 激活方式 使用什么来收集日志。可选择的有：rabbitMq、redis、rocketmq
active.client=rocketmq
# 如果激活方式是rabbitMq 则需要配置rabbitmq 信息
rabbitmq.config.host=localhost
rabbitmq.config.port=5672
rabbitmq.config.virtual-host=/
rabbitmq.config.username=guest
rabbitmq.config.password=guest

# 如果激活的是redis 则需要配置 redis信息
redis.config.host=localhost
redis.config.port=6379
#若没有密码可以不写
redis.config.auth=
redis.config.timeout=5000
redis.config.maxIdle=50
redis.config.maxWaitMillis=3000
redis.config.maxTotal=500
# server端 redis队列为空时阻塞时间 单位秒
redis.sleep.timeout=10

# 如果激活的方式是rocketmq 则需要配置rocketmq的信息
rocketmq.config.namesrv_addr=localhost:9876
# 批量拉取数量
rocketmq.config.pull_batch_size=10
# 配了消费最大数量
rocketmq.config.consume_message_batch_max_size=10
# 拉取
rocketmq.config.pull_threshold_for_queue=1000
# 消费者线程最小数量
rocketmq.config.consume_thread_min=3
# 消费者线程最大数量
rocketmq.config.consume_thread_max=5





# es 配置
elasticsearch.schema=http
elasticsearch.address=127.0.0.1:9200
elasticsearch.connectTimeout=5000
elasticsearch.socketTimeout=5000
elasticsearch.connectionRequestTimeout=5000
elasticsearch.maxConnectNum=100
elasticsearch.maxConnectPerRoute=100
# 若设置了用户名和密码，则需要此处
elasticsearch.user=
# 若设置了用户名和密码，则需要此处
elasticsearch.pwd=

# 创建索引分片数量
elasticsearch.index.shards=3
# 创建索引副本数量
elasticsearch.index.replicas=2

spring.thymeleaf.prefix=classpath:/templates/
spring.thymeleaf.suffix=.html
spring.thymeleaf.mode=HTML5
spring.thymeleaf.cache=false
spring.thymeleaf.encoding=UTF-8

```

![示例截图1](https://images.gitee.com/uploads/images/2021/0814/134614_6803a300_695155.png "屏幕截图.png")

![示例截图2](https://images.gitee.com/uploads/images/2021/0814/134656_ff0a145a_695155.png "屏幕截图.png")

![示例截图3](https://images.gitee.com/uploads/images/2021/0814/134732_24934af8_695155.png "屏幕截图.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

