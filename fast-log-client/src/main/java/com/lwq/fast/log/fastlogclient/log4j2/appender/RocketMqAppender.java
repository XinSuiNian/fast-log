package com.lwq.fast.log.fastlogclient.log4j2.appender;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.lwq.fast.log.fastlogclient.log4j2.appender.util.MessageUtil;
import com.lwq.fast.log.fastlogcore.client.redis.RedisClient;
import com.lwq.fast.log.fastlogcore.client.rocketmq.RocketMqClient;
import com.lwq.fast.log.fastlogcore.entity.Message;
import com.lwq.fast.log.fastlogcore.util.ThreadPoolUtil;
import com.lwq.fast.log.fastlogcore.work.MessageCollection;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

import java.io.Serializable;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 自定义 appender 使用rocketmq 来收集日志
 *
 * @author 刘文强
 */
@Plugin(name = "Log4jRocketMqAppender", category = "Core", elementType = "appender", printObject = true)
public class RocketMqAppender extends AbstractAppender {

    /**
     * 应用名称
     */
    private String appName;
    /**
     * 环境信息  如： dev， test, prod 等
     */
    private String env;


    /**
     * rocketmq namesrvAddr
     */
    private String namesrvAddr;




    protected RocketMqAppender(String name, Filter filter, Layout<? extends Serializable> layout,
                            boolean ignoreExceptions, String appName, String env, String namesrvAddr) {
        super(name, filter, layout, ignoreExceptions, Property.EMPTY_ARRAY);
        this.appName = appName;
        this.env = env;
        this.namesrvAddr = namesrvAddr;
        RocketMqClient instance = RocketMqClient.getInstance(namesrvAddr);
        if (ObjectUtil.isNotNull(instance)){
            // 从队列中获取消息，发送到 MQ中
            ThreadPoolExecutor threadPoolExecutor = ThreadPoolUtil.getThreadPoolExecutor();
            threadPoolExecutor.execute(() -> {
                MessageCollection.messageCollect(instance);
            });
        }
    }


    @Override
    public void append(LogEvent logEvent) {
        Message message = MessageUtil.formatMessage(appName, env, logEvent);
        if (ObjectUtil.isNotNull(message)){
            MessageCollection.addMessage2Queue(JSON.toJSONString(message));
        }

    }


    @PluginFactory
    public static RocketMqAppender createAppender(@PluginAttribute("name") String name,
                                               @PluginElement("filter") Filter filter,
                                               @PluginElement("layout") Layout<? extends Serializable> layout,
                                               @PluginAttribute("ignoreExceptions") boolean ignoreExceptions,
                                               @PluginAttribute("appName") String appName,
                                               @PluginAttribute("env") String env,
                                               @PluginAttribute("namesrvAddr") String namesrvAddr
    ) {
        if (null == name) {
            LOGGER.error("name 为空");
            return null;
        }
        if (null == layout) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new RocketMqAppender(name, filter, layout, ignoreExceptions, appName, env, namesrvAddr);
    }





    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getNamesrvAddr() {
        return namesrvAddr;
    }

    public void setNamesrvAddr(String namesrvAddr) {
        this.namesrvAddr = namesrvAddr;
    }
}
