package com.lwq.fast.log.fastlogcore.client;

/**
 *
 * @author 刘文强
 */
public abstract class AbstractClient {
    private static AbstractClient client;

    public static AbstractClient getClient() {
        return client;
    }

    public static void setClient(AbstractClient abstractClient) {
        client = abstractClient;
    }

    /**
     * 处理日志信息
     *
     * @param message
     */
    public void handlerMessage(String message){}

}
