package com.lwq.fast.log.fastlogcore.client.rabbitmq;

import com.lwq.fast.log.fastlogcore.client.AbstractClient;
import com.lwq.fast.log.fastlogcore.constant.Constants;
import com.lwq.fast.log.fastlogcore.util.AssertUtil;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;


/**
 *
 * @author 刘文强
 */
public class RabbitMqClient extends AbstractClient {
    private static RabbitMqClient instance;
    private String host;
    private int port;
    private String virtualHost;
    private String username;
    private String password;
    private RabbitTemplate rabbitTemplate;

    public RabbitMqClient() {
    }


    public RabbitMqClient(String host, int port, String virtualHost, String username, String password) {
        this.host = host;
        this.port = port;
        this.virtualHost = virtualHost;
        this.username = username;
        this.password = password;
        try {
            RabbitTemplate rabbitTemplate = getRabbitTemplate(host, port, virtualHost, username, password);
            this.rabbitTemplate = rabbitTemplate; // 初始化队列 、 交换机 、 路由键信息
            initBindingInfo(host, port, virtualHost, username, password);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * 初始化交换机、队列、路由键信息
     * @param host
     * @param port
     * @param virtualHost
     * @param username
     * @param password
     */
    private void initBindingInfo(String host, int port, String virtualHost, String username, String password) {
        RabbitAdmin rabbitAdmin = new RabbitAdmin(getConnectionFactory(host, port, virtualHost, username, password));
        rabbitAdmin.setAutoStartup(true);
        rabbitAdmin.declareQueue(new Queue(Constants.QUEUE, true, false, false));
        rabbitAdmin.declareExchange(new DirectExchange(Constants.EXCHANGE, true, false));
        rabbitAdmin.declareBinding(new Binding(Constants.QUEUE, Binding.DestinationType.QUEUE, Constants.EXCHANGE, Constants.ROUTING_KEY, null));
    }

    private RabbitTemplate getRabbitTemplate(String host, int port, String virtualHost, String username, String password) {
        AssertUtil.isNotBlank(host,"host不可为空");
        AssertUtil.isNotBlank(virtualHost,"virtualHost不可为空");
        ConnectionFactory cachingConnectionFactory = getConnectionFactory(host, port, virtualHost, username, password);
        RabbitTemplate rabbitTemplate = new RabbitTemplate(cachingConnectionFactory);
        rabbitTemplate.setExchange(Constants.EXCHANGE);
        rabbitTemplate.setRoutingKey(Constants.ROUTING_KEY);
        return rabbitTemplate;
    }

    /**
     * 获取连接工厂
     * @param host
     * @param port
     * @param virtualHost
     * @param username
     * @param password
     * @return
     */
    private ConnectionFactory getConnectionFactory(String host, int port, String virtualHost, String username, String password) {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory();
        cachingConnectionFactory.setHost(host);
        cachingConnectionFactory.setPort(port);
        cachingConnectionFactory.setVirtualHost(virtualHost);
        cachingConnectionFactory.setUsername(username);
        cachingConnectionFactory.setPassword(password);
        return cachingConnectionFactory;
    }

    /**
     * 获取客户端实例
     * @param host
     * @param port
     * @param virtualHost
     * @param username
     * @param password
     * @return
     */
    public static RabbitMqClient getInstance(String host, int port, String virtualHost, String username, String password) {
        if (null == instance) {
            synchronized (RabbitMqClient.class) {
                if (null == instance) {
                    instance = new RabbitMqClient(host, port, virtualHost, username, password);
                }
            }
        }
        return instance;
    }


    /**
     * 处理日志信息
     *
     * @param message
     */
    @Override
    public void handlerMessage(String message) {
        rabbitTemplate.convertAndSend(message);
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getVirtualHost() {
        return virtualHost;
    }

    public void setVirtualHost(String virtualHost) {
        this.virtualHost = virtualHost;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RabbitTemplate getRabbitTemplate() {
        return rabbitTemplate;
    }

    public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }
}
