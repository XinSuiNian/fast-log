package com.lwq.fast.log.fastlogcore.client.rocketmq;

import cn.hutool.core.text.StrBuilder;
import com.lwq.fast.log.fastlogcore.client.AbstractClient;
import com.lwq.fast.log.fastlogcore.constant.Constants;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;


/**
 *
 * @author 刘文强
 */
public class RocketMqClient extends AbstractClient {

    private static RocketMqClient instance;
    DefaultMQProducer producer;

    private String namesrvAddr;

    private RocketMqClient(String namesrvAddr){
        try {
            producer = new DefaultMQProducer(Constants.ROCKETMQ_PRODUCER_GROUP);
            producer.setNamesrvAddr(namesrvAddr);
            producer.start();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static RocketMqClient getInstance(String namesrvAddr){
        if (null == instance){
            synchronized (RocketMqClient.class){
                if (null == instance){
                    instance = new RocketMqClient(namesrvAddr);
                }
            }
        }
        return instance;
    }


    public static RocketMqClient getInstanceForClose(){
        return instance;
    }
    /**
     * 处理日志信息
     *
     * @param message
     */
    @Override
    public void handlerMessage(String message) {
        try {
            Message mqMsg = new Message(Constants.ROCKETMQ_MESSAGE_TOPIC,message.getBytes());
            producer.sendOneway(mqMsg);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public DefaultMQProducer getProducer() {
        return producer;
    }

    public String getNamesrvAddr() {
        return namesrvAddr;
    }

    public void setNamesrvAddr(String namesrvAddr) {
        this.namesrvAddr = namesrvAddr;
    }
}
