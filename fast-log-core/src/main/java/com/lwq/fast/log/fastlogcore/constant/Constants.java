package com.lwq.fast.log.fastlogcore.constant;

/**
 *
 * @author 刘文强
 */
public class Constants {
    /**
     * 队列名称
     */
    public static final String QUEUE = "fast.log.queue";

    /**
     * 交换机
     */
    public static final String EXCHANGE = "fast.log.exchange";

    /**
     * 路由键
     */
    public static final String ROUTING_KEY = "fast.log.routing.key";


    /**
     * 线程池常量
     */
    public static class Thread{
        /**
         * 核心线程数
         */
        public static int CORE_POOL_SIZE = 2;
        /**
         * 最大线程数
         */
        public static int MAX_POOL_SIZE = 4;
        /**
         * 存活时间
         */
        public static long KEEP_ALIVE_TIME = 300L;
        /**
         * 线程池队列大小
         */
        public static int CAPACITY = 10000;
    }

    /**
     * 暂存日志队列大小
     */
    public static int WORKER_QUEUE_CAPACITY = 10000;

    /**
     * appname信息索引
     */
    public static final String APP_NAME_INDEX = "fast_log_app_name";

    /**
     * 环境信息索引
     */
    public static final String APP_ENV_INDEX = "fast_log_app_env";


    /**
     * 链路id
     */
    public static final String TRACE_ID = "TRACE_ID";


    /**
     * 在redis队列中的名称
     */
    public static final String REDIS_LOG_KEY = "fast:log:list";


    /**
     * rocketmq 生产者组
     */
    public static final String ROCKETMQ_PRODUCER_GROUP = "fast_log_producer_group";

    /**
     * rocketmq 消息主题
     */
    public static final String ROCKETMQ_MESSAGE_TOPIC = "fast_log_msg_topic";

    /**
     * rocketmq 消费者组
     */
    public static final String ROCKETMQ_CONSUMER_GROUP = "fast_log_consumer_group";







}
