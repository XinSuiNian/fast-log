package com.lwq.fast.log.fastlogcore.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 刘文强
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AppEnvIndex implements Serializable {

    /**
     * 环境信息
     */
    private String env;

    /**
     * 环境描述
     */
    private String envDesc;
}
