package com.lwq.fast.log.fastlogcore.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 刘文强
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppNameIndex implements Serializable {

    /**
     * 应用名称
     */
    private String appName;

}
