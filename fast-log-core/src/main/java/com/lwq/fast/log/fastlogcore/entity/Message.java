package com.lwq.fast.log.fastlogcore.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


/**
 * 日志详细信息
 *
 * @author  刘文强
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Message {

    /**
     * 应用名称
     */
    private String appName;

    /**
     * 环境信息 如： dev， test, prod 等
     */
    private String env;

    /**
     * 环境信息描述，最长15个字符，超出截取前15个。 如：测试环境， 开发环境
     */
    private String envDesc;

    /**
     * 线程名称
     */
    private String threadName;

    /**
     * 日志级别
     */
    private String level;
    /**
     * 链路id
     */
    private String traceId;
    /**
     * 输出日志类名
     */
    private String className;
    /**
     * 日志内容
     */
    private String content;

    /**
     * 时间
     */
    private Date dateTime;

}
