package com.lwq.fast.log.fastlogcore.trace;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.ttl.TransmittableThreadLocal;


/**
 * 链路 TransmittableThreadLocal
 *
 * @author 刘文强
 */
public class TraceThreadLocal {

    // TransmittableThreadLocal 完成父线程到子线程的值传递 详情 可参见：https://github.com/alibaba/transmittable-thread-local
    private static TransmittableThreadLocal<String> traceThreadLocal = new TransmittableThreadLocal<>();

    /**
     * 获取traceId
     *
     * @return
     */
    public static String get(){
        return traceThreadLocal.get();
    }

    /**
     * 设置 traceId
     *
     * @param traceId
     */
    public static void set(String traceId){
        traceThreadLocal.set(StrUtil.isBlank(traceId) ? IdUtil.simpleUUID() : traceId );
    }


    /**
     * 移除traceId
     */
    public static void remove(){
        traceThreadLocal.remove();
    }



}
