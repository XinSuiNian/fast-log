package com.lwq.fast.log.fastlogcore.util;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.lwq.fast.log.fastlogcore.exception.CommonException;

import java.util.Collection;
import java.util.Map;

/**
 * 断言工具类
 *
 * @author 刘文强
 */
public class AssertUtil {


    /**
     * 断言字符串不为空，若为空抛出异常
     * @param str 断言的字符串
     * @param errorMsg 错误信息
     */
    public static void isNotBlank(String str, String errorMsg)  {
        if (StrUtil.isBlank(str)){
            throw new CommonException(errorMsg);
        }
    }


    /**
     * 断言字符串为空，若不为空抛出异常
     * @param str 断言的字符串
     * @param errorMsg 错误信息
     */
    public static void isBlank(String str, String errorMsg)  {
        if (StrUtil.isNotBlank(str)){
            throw new CommonException(errorMsg);
        }
    }


    /**
     * 断言对象为空，若不为空抛出异常
     * @param obj 断言的对象
     * @param errorMsg 错误信息
     */
    public static void isNull(Object obj, String errorMsg)  {
        if (ObjectUtil.isNotNull(obj)){
            throw new CommonException(errorMsg);
        }
    }

    /**
     * 断言对象串不为空，若为空抛出异常
     * @param obj 断言的对象
     * @param errorMsg 错误信息
     */
    public static void isNotNull(Object obj, String errorMsg)  {
        if (ObjectUtil.isNull(obj)){
            throw new CommonException(errorMsg);
        }
    }

    /**
     * 断言集合不为空，若为空抛出异常
     * @param collection 断言的集合
     * @param errorMsg 错误信息
     */
    public static void isNotEmpty(Collection<?> collection, String errorMsg)  {
        if (CollectionUtil.isEmpty(collection)){
            throw new CommonException(errorMsg);
        }
    }

    /**
     * 断言集合为空，若不为空抛出异常
     * @param collection 断言的集合
     * @param errorMsg 错误信息
     */
    public static void isEmpty(Collection<?> collection, String errorMsg)  {
        if (CollectionUtil.isNotEmpty(collection)){
            throw new CommonException(errorMsg);
        }
    }

    /**
     * 断言map为空，若不为空抛出异常
     * @param map 断言的map
     * @param errorMsg 错误信息
     */
    public static void isEmpty(Map<?, ?> map, String errorMsg)  {
        if (CollectionUtil.isNotEmpty(map)){
            throw new CommonException(errorMsg);
        }
    }

    /**
     * 断言map不为空，若为空抛出异常
     * @param map 断言的map
     * @param errorMsg 错误信息
     */
    public static void isNotEmpty(Map<?, ?> map, String errorMsg)  {
        if (CollectionUtil.isEmpty(map)){
            throw new CommonException(errorMsg);
        }
    }



}
