package com.lwq.fast.log.fastlogcore.util;

import com.lwq.fast.log.fastlogcore.constant.Constants;

import java.util.concurrent.*;

/**
 * 线程池工具类
 * @author 刘文强
 */
public class ThreadPoolUtil {



    public static ThreadPoolExecutor getThreadPoolExecutor(){
        return new ThreadPoolExecutor(Constants.Thread.CORE_POOL_SIZE,
                Constants.Thread.MAX_POOL_SIZE, Constants.Thread.KEEP_ALIVE_TIME, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(Constants.Thread.CAPACITY),
                new ThreadPoolExecutor.DiscardOldestPolicy()
        );
    }
}
