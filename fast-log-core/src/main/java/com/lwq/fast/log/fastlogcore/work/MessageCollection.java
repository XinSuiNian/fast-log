package com.lwq.fast.log.fastlogcore.work;

import com.lwq.fast.log.fastlogcore.client.AbstractClient;
import com.lwq.fast.log.fastlogcore.client.rabbitmq.RabbitMqClient;
import com.lwq.fast.log.fastlogcore.constant.Constants;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * 日志收集
 * @author 刘文强
 */
public class MessageCollection {

    /**
     * 暂存日志信息的队列
     */
    public static LinkedBlockingQueue<String> logQueue = new LinkedBlockingQueue<>(Constants.WORKER_QUEUE_CAPACITY);


    /**
     * 初始化队列
     */
    public static void initLogQueue(){
        if (null == logQueue){
            logQueue = new LinkedBlockingQueue<>(Constants.WORKER_QUEUE_CAPACITY);
        }
    }

    /**
     * 将日志放入队列
     *
     * @param message 日志信息
     */
    public static void addMessage2Queue(String message){
        initLogQueue();
        try{
            logQueue.add(message);
        }catch ( IllegalStateException e){
            // 说明队列满了
            logQueue.clear();
        }
    }

    /**
     * 日志信息收集。阻塞队列中的日志取出，放入到Mq中
     *
     * @param client
     */
    public static void messageCollect(AbstractClient client) {
        initLogQueue();
        while (true){
            // 从队列中取出数据
            try {
                // 使用阻塞队列， 防止空转 占用cpu
                String message = logQueue.take();
                client.handlerMessage(message);
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }





}
