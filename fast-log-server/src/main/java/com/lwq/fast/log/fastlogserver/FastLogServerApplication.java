package com.lwq.fast.log.fastlogserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class FastLogServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FastLogServerApplication.class, args);
    }

}
