package com.lwq.fast.log.fastlogserver.common;

import lombok.Data;

import java.io.Serializable;

/**
 * 分页请求参数
 *
 * @author 刘文强
 */
@Data
public class EsPageRequest implements Serializable {

    /**
     * 当前页
     */
    private Integer pageNum = 1;
    /**
     * 每页多少条记录
     */
    private Integer pageSize = 10;
}
