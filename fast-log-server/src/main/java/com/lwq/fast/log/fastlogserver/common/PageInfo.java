package com.lwq.fast.log.fastlogserver.common;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页公用实体
 *
 * @author 刘文强
 * @param <T>
 */

@Data
public class PageInfo<T> implements Serializable {
    /**
     * 总记录数
     */
    private long total;
    /**
     * 当前页
     */
    private int pageNum;
    /**
     * 每页暂时多少条数据
     */
    private int pageSize;
    /**
     * 返回数据
     */
    private List<T> rows;
}
