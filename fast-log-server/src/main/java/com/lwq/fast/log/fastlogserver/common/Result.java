package com.lwq.fast.log.fastlogserver.common;


import lombok.Data;

/**
 * 统一返回结果
 *
 * @param <T>
 * @author 刘文强
 */

@Data
public class Result<T> {

    public static final String SUCCESS_CODE = "success";

    public static final String ERROR_CODE = "error";

    public static final String SUCCESS_MESSAGE = "success";

    public static final String ERROR_MESSAGE = "error";


    /**
     * 状态码
     */
    private String code;
    /**
     * 结果描述
     */
    private String message;
    /**
     * 消息信息
     */
    private T body;

    public static<T> Result<T> success() {
        return new Result();
    }

    public static<T> Result<T> success(String message) {
        return success(message, null);
    }

    public static<T> Result<T> success(T body) {
        return success(SUCCESS_MESSAGE, body);
    }

    public static<T> Result<T> success(String message, T body) {
        return success(SUCCESS_CODE, message, body);
    }

    public static<T> Result<T> success(String code, String message, T body) {
        Result<T> result = new Result<>();
        result.setBody(body);
        result.setCode(code);
        result.setMessage(message);
        return result;
    }


    public static<T> Result<T> error(String code, String message) {
        Result<T> result = new Result<>();
        result.setCode(code);
        result.setMessage(message);
        return result;
    }
    public static<T> Result<T> error(String message) {
       return error(ERROR_CODE,message);
    }


}
