package com.lwq.fast.log.fastlogserver.common.util;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;


/**
 * 应用名称和环境信息缓存
 *
 * @author 刘文强
 */
public class AppNameEnvCacheUtil {


    /**
     * 缓存过期时间24h （单位毫秒）
     */
    private static final long TIME_OUT = 1000 * 60 * 60 * 24;

    /**
     * 应用名称缓存
     */
    private static TimedCache<String, String> appNameCache = CacheUtil.newTimedCache(TIME_OUT);

    /**
     * 环境名称缓存
     */
    private static TimedCache<String, String> envCache = CacheUtil.newTimedCache(TIME_OUT);

    /**
     * 索引名称缓存
     */
    private static TimedCache<String, String> indicesCache = CacheUtil.newTimedCache(TIME_OUT);


    /**
     * 获取应用名称缓存信息
     *
     * @return
     */
    public static String  getAppNameCacheByKey(String key) {
        return appNameCache.get(key);
    }

    /**
     * 设置应用名称缓存
     *
     * @param key
     * @param value
     */
    public static void setAppNameCache(String key, String value){
        appNameCache.put(key, value);
    }


    /**
     * 获取环境名称缓存信息
     *
     * @return
     */
    public static String getEnvCacheByKey(String key) {
        return envCache.get(key);
    }

    /**
     * 设置环境名称缓存信息
     *
     * @param key
     * @param value
     */
    public static void setEnvCache(String key, String value) {
       envCache.put(key,value);
    }


    /**
     * 获取索引
     *
     * @param key
     * @return
     */
    public static String getIndicesCacheByKey(String key){
        return indicesCache.get(key);
    }

    /**
     * 设置索引缓存
     *
     * @param key
     * @param value
     */
    public static void setIndicesCache(String key, String value){
        indicesCache.put(key,value);
    }
}
