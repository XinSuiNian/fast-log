package com.lwq.fast.log.fastlogserver.es;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import org.elasticsearch.index.query.MatchPhraseQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.index.query.WildcardQueryBuilder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ElasticSearchUtils {

    private static final String WILDCARD_CHARACTERS = "*";


    /**
     * 生成 terms 条件
     * @param fieldName 字段
     * @param value 值
     * @return
     */
    public Optional<TermsQueryBuilder> buildTerms(String fieldName, String value) {
        if (StrUtil.isNotBlank(value)) {
            TermsQueryBuilder termsQueryBuilder = new TermsQueryBuilder(fieldName, value);
            return Optional.of(termsQueryBuilder);
        }
        return Optional.empty();
    }

    /**
     * 生成 term 条件
     * @param fieldName 字段
     * @param value 值
     * @return
     */
    public Optional<TermQueryBuilder> buildTerm(String fieldName, String value) {
        if (StrUtil.isNotBlank(value)) {
            TermQueryBuilder termQueryBuilder = new TermQueryBuilder(fieldName, value);
            return Optional.of(termQueryBuilder);
        }
        return Optional.empty();
    }

    /**
     * 生成 MatchPhrase 条件
     * @param fieldName 字段
     * @param value 值
     * @return
     */
    public Optional<MatchPhraseQueryBuilder> buildMatchPhrase(String fieldName, String value){
        if (StrUtil.isNotBlank(value)){
            MatchPhraseQueryBuilder matchPhraseQueryBuilder = new MatchPhraseQueryBuilder(fieldName,value);
            return Optional.of(matchPhraseQueryBuilder);
        }
        return Optional.empty();
    }

    /**
     * 生成queryString 条件
     *
     * @param filedName 字段
     * @param value 值
     * @return
     */
    public Optional<QueryStringQueryBuilder> buildQueryString(String filedName, String value){
        return buildQueryString(filedName,value, true, true);
    }
    
    /**
     * 生成queryString 条件
     *
     * @param filedName 字段
     * @param value 值
     * @param prefixWildcard 前通配符
     * @param subfixWildcard 后通配符
     * @return
     */
    public Optional<QueryStringQueryBuilder> buildQueryString(String filedName, String value, Boolean prefixWildcard, Boolean subfixWildcard){
        if (StrUtil.isBlank(value)){
            return Optional.empty();
        }
        if (BooleanUtil.isTrue(prefixWildcard)){
            value = WILDCARD_CHARACTERS + value;
        }
        if (BooleanUtil.isTrue(subfixWildcard)){
            value = value + WILDCARD_CHARACTERS;
        }
        QueryStringQueryBuilder queryStringQueryBuilder = new QueryStringQueryBuilder(value);
        queryStringQueryBuilder.defaultField(filedName);
        queryStringQueryBuilder.analyzeWildcard(true);
        return Optional.of(queryStringQueryBuilder);
    }

    public Optional<WildcardQueryBuilder> buildWildcard(String filedName, String value){
        return buildWildcard(filedName, value, true, true);
    }
    public Optional<WildcardQueryBuilder> buildWildcard(String filedName, String value, Boolean prefixWildcard, Boolean subfixWildcard){

        if (StrUtil.isBlank(value)){
            return Optional.empty();
        }
        if (BooleanUtil.isTrue(prefixWildcard)){
            value = WILDCARD_CHARACTERS + value;
        }
        if (BooleanUtil.isTrue(subfixWildcard)){
            value = value + WILDCARD_CHARACTERS;
        }
        WildcardQueryBuilder wildcardQueryBuilder = QueryBuilders.wildcardQuery(filedName, value);

        return Optional.of(wildcardQueryBuilder);
    }
}
