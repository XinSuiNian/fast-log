package com.lwq.fast.log.fastlogserver.es.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lwq.fast.log.fastlogserver.common.EsPageRequest;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;

/**
 * 查询请求参数
 *
 * @author  刘文强
 */
@Data
public class QueryRequest extends EsPageRequest {

    /**
     * 应用名称
     */
    private List<String> appNameList;

    /**
     * 环境信息 如： dev， test, prod 等
     */
    private String env;

    /**
     * 日志级别
     */
    private String level;
    /**
     * 链路id
     */
    private String traceId;
    /**
     * 日志内容
     */
    private String content;
    /**
     * 开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    /**
     * 结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
}
