package com.lwq.fast.log.fastlogserver.es.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 查询日志返回信息
 *
 * @author 刘文强
 */
@Data
public class LogInfoResponse implements Serializable {

    /**
     * 应用名称
     */
    private String appName;

    /**
     * 线程
     */
    private String threadName;

    /**
     * 日志级别
     */
    private String level;
    /**
     * 链路id
     */
    private String traceId;
    /**
     * 输出日志类名
     */
    private String className;
    /**
     * 日志内容
     */
    private String content;
    /**
     * 时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS" , timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS" )
    private Date dateTime;
}
