package com.lwq.fast.log.fastlogserver.es.service;

import com.lwq.fast.log.fastlogcore.entity.AppEnvIndex;
import com.lwq.fast.log.fastlogcore.entity.Message;
import com.lwq.fast.log.fastlogserver.common.PageInfo;
import com.lwq.fast.log.fastlogserver.es.request.QueryRequest;

import java.io.IOException;
import java.util.List;

/**
 * @author 刘文强
 */
public interface ElasticSearchService {

    /**
     * 将日志信息写入 ES
     *
     * @param logMessage
     */
    void logMessage2Es(String logMessage) throws IOException;


    /**
     * 检查索引是否存在 存在返回true,否则返回false
     *
     * @param index 索引名称
     * @return boolean
     */
    boolean existIndex(String index) throws IOException;


    /**
     * 创建日志索引
     *
     * @param index 索引名称
     * @return
     */
    boolean createLogIndex(String index) throws IOException;


    /**
     * 添加文档信息到索引中
     *
     * @param index 索引名称
     * @param jsonDocument 文档信息-json格式
     * @return
     */
    int addLog2Index(String index, String jsonDocument) throws IOException;

    /**
     * 获取所有应用名称
     *
     * @return {@code List<String>}
     */
    List<String> getAllAppNames() throws IOException;

    /**
     * 获取全部环境信息
     *
     * @return {@code List<AppEnvIndex>}
     */
    List<AppEnvIndex> getAllEnv() throws IOException;

    /**
     * 根据条件查询数据
     *
     * @param request 请求参数
     * @return PageInfo
     */
    PageInfo queryData(QueryRequest request) throws IOException;

    /**
     * 获取全部索引名称
     *
     * @return
     */
    List<String> getAllIndices() throws IOException;

    /**
     * 将日志信息批量写入 ES
     *
     * @param messageList
     */
    void logMessage2EsBatch(List<Message> messageList) throws Exception;
}
