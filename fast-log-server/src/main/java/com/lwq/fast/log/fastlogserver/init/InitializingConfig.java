package com.lwq.fast.log.fastlogserver.init;

import com.lwq.fast.log.fastlogserver.init.service.IndexInitializeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 *初始化配置信息
 *
 * @author  刘文强
 */
@Component
@Slf4j
public class InitializingConfig implements InitializingBean {


    @Autowired
    private IndexInitializeService indexInitializeService;

    @Override
    public void afterPropertiesSet()  {
        try{
            //初始化默认索引
            indexInitializeService.initializeIndex();
        }catch (Exception e){
           log.error("初始化数据出错",e);
        }
    }
}

