package com.lwq.fast.log.fastlogserver.init.service;

/**
 * 索引初始化
 * @author  刘文强
 */
public interface IndexInitializeService {

    /**
     * 初始化默认索引信息
     */
    void initializeIndex() throws Exception;
}
