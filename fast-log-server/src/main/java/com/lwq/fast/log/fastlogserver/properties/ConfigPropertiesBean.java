package com.lwq.fast.log.fastlogserver.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 *
 * 配置信息Bean
 * @author 刘文强
 */
@Component
@Data
public class ConfigPropertiesBean {

    /**
     * 创建索引分片数量
     */
    @Value("${elasticsearch.index.shards}")
    public int shards;
    /**
     * 创建索引副本数量
     */
    @Value("${elasticsearch.index.replicas}")
    public int replicas;

}
