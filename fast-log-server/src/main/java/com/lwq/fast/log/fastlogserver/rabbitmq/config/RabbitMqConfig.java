package com.lwq.fast.log.fastlogserver.rabbitmq.config;


import com.lwq.fast.log.fastlogcore.constant.Constants;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * rabbitmq 配置类
 *
 * @author 刘文强
 */
@Configuration
@ConditionalOnProperty(name = "active.client", havingValue = "rabbitMq")
public class RabbitMqConfig {
    @Value("${rabbitmq.config.host}")
    private String host;
    @Value("${rabbitmq.config.port}")
    private int port;
    @Value("${rabbitmq.config.virtual-host}")
    private String virtualHost;
    @Value("${rabbitmq.config.username}")
    private String username;
    @Value("${rabbitmq.config.password}")
    private String password;

    @Value("${rabbitmq.config.prefetchCount}")
    private Integer prefetchCount;
    @Value("${rabbitmq.config.batchSize}")
    private Integer batchSize;
    @Value("${rabbitmq.config.concurrentConsumers}")
    private Integer concurrentConsumers;
    @Value("${rabbitmq.config.maxConcurrentConsumers}")
    private Integer maxConcurrentConsumers;

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost(host);
        connectionFactory.setPort(port);
        connectionFactory.setVirtualHost(virtualHost);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        return connectionFactory;
    }

    @Bean
    @ConditionalOnBean(ConnectionFactory.class)
    public RabbitAdmin rabbitAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

   /* @Bean
    @ConditionalOnProperty(name = "active.client", havingValue = "rabbitMq")
    public ChannelAwareMessageListener consumerListener() {
        return new RabbitConsumerListener();
    }*/

    /*@Bean
    @ConditionalOnBean({ConnectionFactory.class, RabbitAdmin.class, ChannelAwareMessageListener.class})
    public MessageListenerContainer messageListenerContainer() {
        SimpleMessageListenerContainer messageListenerContainer = new SimpleMessageListenerContainer();
        messageListenerContainer.setConnectionFactory(connectionFactory());
        messageListenerContainer.setAmqpAdmin(rabbitAdmin());
        messageListenerContainer.setPrefetchCount(10);
        messageListenerContainer.setBatchSize(10);
        messageListenerContainer.setConcurrency("5-10");
        messageListenerContainer.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        messageListenerContainer.setMessageListener(consumerListener());
        messageListenerContainer.setQueues(getQueue());
        return messageListenerContainer;
    }*/


    @Bean
    SimpleRabbitListenerContainerFactory consumerBatchContainerFactory() {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setBatchListener(true);
        // factory.setReceiveTimeout(1000L * 3);
        factory.setConsumerBatchEnabled(true);
        factory.setPrefetchCount(prefetchCount);
        if (batchSize > prefetchCount){
            batchSize = prefetchCount;
        }
        factory.setBatchSize(batchSize);
        if (concurrentConsumers > maxConcurrentConsumers){
            concurrentConsumers = maxConcurrentConsumers;
        }
        factory.setConcurrentConsumers(concurrentConsumers);
        factory.setMaxConcurrentConsumers(maxConcurrentConsumers);
        factory.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        return factory;
    }

    @Bean
    public DirectExchange directExchange() {
        DirectExchange directExchange = new DirectExchange(Constants.EXCHANGE, true, false);
        directExchange.setAdminsThatShouldDeclare(rabbitAdmin());
        return directExchange;
    }

    @Bean
    public Queue getQueue() {
        Queue queue = new Queue(Constants.QUEUE, true, false, false);
        queue.setAdminsThatShouldDeclare(rabbitAdmin());
        return queue;
    }

    @Bean
    public Binding directQueueBinding() {
        Binding binding = new Binding(Constants.QUEUE, Binding.DestinationType.QUEUE, Constants.EXCHANGE, Constants.ROUTING_KEY, null);
        binding.setAdminsThatShouldDeclare(rabbitAdmin());
        return binding;
    }
}