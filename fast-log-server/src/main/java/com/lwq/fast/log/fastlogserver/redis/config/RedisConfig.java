package com.lwq.fast.log.fastlogserver.redis.config;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
@ConditionalOnProperty(name = "active.client", havingValue = "redis")
public class RedisConfig {

    @Value("${redis.config.host}")
    private String host;
    @Value("${redis.config.port}")
    private int port;
    @Value("${redis.config.auth}")
    private String auth;

    @Value("${redis.config.timeout}")
    private int timeOut;

    @Value("${redis.config.maxIdle}")
    private int maxIdle;

    @Value("${redis.config.maxWaitMillis}")
    private long maxWaitMillis;

    @Value("${redis.config.maxTotal}")
    private int maxTotal;


    @Bean
    public JedisPool getJedisPoll(){
        JedisPool jedisPool = null;
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(maxTotal);
        config.setMaxIdle(maxIdle);
        config.setMaxWaitMillis(maxWaitMillis);
        if(StrUtil.isBlank(auth)){
            jedisPool = new JedisPool(config, host, port, timeOut);
        }else{
            jedisPool = new JedisPool(config, host, port, timeOut, auth);
        }
        return jedisPool;
    }


}
