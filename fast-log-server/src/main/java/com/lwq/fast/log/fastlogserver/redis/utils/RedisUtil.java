package com.lwq.fast.log.fastlogserver.redis.utils;

import com.lwq.fast.log.fastlogcore.constant.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPool;

import java.util.List;

@Component
@ConditionalOnProperty(name = "active.client", havingValue = "redis")
public class RedisUtil {

    private int timeout = 5000;

    @Autowired
    private JedisPool jedisPool;


    public List<String> brpop(){
        List<String> message = jedisPool.getResource().brpop(timeout, Constants.REDIS_LOG_KEY);
        return message;
    }
}
