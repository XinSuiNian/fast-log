package com.lwq.fast.log.fastlogserver.ui.controller;

import com.lwq.fast.log.fastlogserver.common.PageInfo;
import com.lwq.fast.log.fastlogserver.common.Result;
import com.lwq.fast.log.fastlogserver.es.request.QueryRequest;
import com.lwq.fast.log.fastlogserver.es.service.ElasticSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

@Controller
public class IndexController {


    @Autowired
    private ElasticSearchService elasticSearchService;

    @RequestMapping("index")
    public String index (){
        return "index";
    }
    @RequestMapping("test")
    public String test (){
        return "test";
    }


    /**
     * 根据条件查询数据
     *
     * @param request 请求参数
     * @return PageInfo
     */
    @PostMapping("queryData")
    @ResponseBody
    public PageInfo queryData(@RequestBody QueryRequest request) throws Exception{
        return elasticSearchService.queryData(request);
    }


    /**
     * 获取全部应用 信息
     *
     * @return Result
     * @throws IOException
     */
    @RequestMapping("getAllAppNames")
    @ResponseBody
    public Result getAllAppNames() throws IOException {
        return Result.success(elasticSearchService.getAllAppNames());
    }

    /**
     * 获取全部环境信息
     *
     * @return Result
     * @throws IOException
     */
    @RequestMapping("getAllEnv")
    @ResponseBody
    public Result getAllEnv() throws IOException {
        return Result.success(elasticSearchService.getAllEnv());
    }
}
