package com.lwq.fast.log.fastlogserver.ui.response.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class IndexDataVO implements Serializable {
    /**
     * 应用名称
     */
    private String appName;

    /**
     * 环境信息 如： dev， test, prod 等
     */
    private String env;

    /**
     * 环境信息描述，最长15个字符，超出截取前15个。 如：测试环境， 开发环境
     */
    private String envDesc;

    /**
     * 日志级别
     */
    private String level;
    /**
     * 链路id
     */
    private String traceId;
    /**
     * 输出日志类名
     */
    private String className;
    /**
     * 日志内容
     */
    private String content;
    /**
     * 时间
     */
    private Date dateTime;

}
