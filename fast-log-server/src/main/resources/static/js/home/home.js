// 开始时间
var startTime;
// 结束时间
var endTime;
// 时间标签 1-近五分钟， 2-近十五分钟， 3-近半小时 ，4-近一小时，5-今天， 6-昨天，7-七天前， 8-当前月，9-上个月
var timeLabel;


$(document).ready(function(){


    var pageNum;
    var pageSize;



    // 预定义的日期范围
    // 最近15分钟开始
    // var start = moment().subtract(15, 'minute');
    // var end = moment();
    startTime =moment().subtract(15, 'minute').format('YYYY-MM-DD HH:mm:ss');
    endTime = moment().format('YYYY-MM-DD HH:mm:ss');

    $('#reportrange').daterangepicker({
        startDate: moment().subtract(15, 'minute'),
        endDate: moment(),
        timePicker24Hour: true, //时间制
        timePickerSeconds: true, //时间显示到秒
        format: 'YYYY-MM-DD HH:mm:ss',
        timePicker:true, // 可以选择时分秒时间
        locale: {
            applyLabel: '确定',
            cancelLabel: '取消',
            fromLabel: '起始时间',
            format: "YYYY-MM-DD HH:mm:ss",
            toLabel: '结束时间',
            customRangeLabel: '手动选择',
            daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
            monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
                '七月', '八月', '九月', '十月', '十一月', '十二月'
            ],
            firstDay: 1
        },
        ranges: {
            '近五分钟': [moment().subtract(5, 'minute'), moment()],
            '近十五分钟': [moment().subtract(15, 'minute'), moment()],
            '近半小时': [moment().subtract(30, 'minute'), moment()],
            '近一小时': [moment().subtract(1, 'hour'), moment()],
            '今天': [moment().startOf('day'), moment().endOf('day')],
            '昨天': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
            '七天前': [moment().subtract(6, 'days'), moment()],
            '当前月': [moment().startOf('month'), moment().endOf('month')],
            '上个月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, function (start, end, label) {
        //选择回调
        if ('近五分钟' == label){
            timeLabel = 1;
            $('#reportrange span').html("近五分钟");
        }else if ("近十五分钟" == label){
            timeLabel = 2;
            $('#reportrange span').html("近十五分钟");
        }else if ("近半小时" == label){
            timeLabel = 3;
            $('#reportrange span').html("近半小时");
        }else if ("近一小时" == label){
            timeLabel = 4;
            $('#reportrange span').html("近一小时");
        }else if ("今天" == label){
            timeLabel = 5;
            $('#reportrange span').html("今天");
        }else if ("昨天" == label){
            timeLabel = 6;
            $('#reportrange span').html("昨天");
        }else if ("七天前" == label){
            timeLabel = 7;
            $('#reportrange span').html("七天前");
        }else if ("当前月" == label){
            timeLabel = 8;
            $('#reportrange span').html("当前月");
        }else if ("上个月" == label){
            timeLabel = 9;
            $('#reportrange span').html("上个月");
        }else{
            //$('#reportrange span').html(start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss'));
            startTime = start.format('YYYY-MM-DD HH:mm:ss');
            endTime = end.format('YYYY-MM-DD HH:mm:ss');
            $('#reportrange span').html(startTime + ' - ' + endTime);
            timeLabel = undefined;
        }

    });




    // 初始化环境信息
    $.ajax({
        url:"getAllAppNames",
        type:"get",
        dataType: "json",
        success : function (data){
            var result = data.body;
            if (typeof (result) != "undefined"){
                for (var i in result){
                    $("#app-name-select").append("<option value='"+result[i]+"'>"+result[i]+"</option>")
                }
                // 因为是动态加载数据，所以需要刷新下。否则不会生效
                $("#app-name-select").trigger("chosen:updated");
            }

        }
    });





    // 初始化环境信息
    $.ajax({
        url:"getAllEnv",
        type:"get",
        dataType: "json",
        success : function (data){
            var result = data.body;
            if (typeof (result) != "undefined"){
                for (var i in result){
                    $("#env-select").append("<option value='"+result[i].env+"'>"+result[i].envDesc+"</option>")
                }
            }

        }
    });




    // 查询
    $("#btn_query").click(function () {
        var appName = $('#app-name-select').val();
        if (isBlank(appName)){
            alert("请选择应用名称");
            return;
        }
        var env = $("#env-select").val();
        if (isBlank(env)){
            alert("请选择环境信息")
            return;
        }


        //初始化查询数据数据
        var oTable = new TableInit();
        oTable.Init();


    });






});


function isBlank(str){
    if (str == null || str == '' || typeof(str) == "undefined" ){
        return true;
    }
    return false;
}

function isNotBlank(str){
    return !isBlank(str);
}


var TableInit = function () {
    var oTableInit = new Object();
    //初始化Table
    oTableInit.Init = function () {
        $('#my-data-table').bootstrapTable('destroy').bootstrapTable({
            url: '/queryData',         //请求后台的URL（*）
            method: 'post',                      //请求方式（*）
            //toolbar: '#toolbar',                //工具按钮用哪个容器
            striped: true,                      //是否显示行间隔色
            cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
            pagination: true,                   //是否显示分页（*）
            sortable: false,                     //是否启用排序
            sortOrder: "asc",                   //排序方式
            queryParams: oTableInit.queryParams,//传递参数（*）
            sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
            pageNumber: 1,                       //初始化加载第一页，默认第一页
            pageSize: 10,                       //每页的记录行数（*）
            pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
            search: false,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            contentType: "application/json",
            strictSearch: true,
            showColumns: true,                  //是否显示所有的列
            showRefresh: false,                  //是否显示刷新按钮
            minimumCountColumns: 2,             //最少允许的列数
            clickToSelect: true,                //是否启用点击选中行
            //height: 700,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
            uniqueId: "no",                     //每一行的唯一标识，一般为主键列
            showToggle: false,                    //是否显示详细视图和列表视图的切换按钮
            cardView: false,                    //是否显示卡片视图
            detailView: false,                   //是否显示父子表
            onLoadSuccess: function (response) {
                console.log(response);
            },
            columns: [
                {
                    field: 'appName',
                    title: '应用名称'
                },
                {
                    field: 'level',
                    title: '日志级别'
                },
                {
                    field: 'threadName',
                    title: '线程'
                },
                {
                    field: 'traceId',
                    title: '链路id'
                },
                {
                    field: 'className',
                    title: '类名称',
                },
                {
                    field: 'content',
                    title: '内容',
                },
                {
                    field: 'dateTime',
                    title: '时间',
                },
                {
                    field: 'operate',
                    title: '操作',
                    //formatter: operateFormatter //自定义方法，添加操作按钮
                },
            ],
            rowStyle: function (row, index) {
                var classesArr = ['success', 'info'];
                var strclass = "";
                if (index % 2 === 0) {//偶数行
                    strclass = classesArr[0];
                } else {//奇数行
                    strclass = classesArr[1];
                }
                return {classes: strclass};
            },//隔行变色
        });

    };


    //得到查询的参数
    oTableInit.queryParams = function (params) {

        // 时间标签 1-近五分钟， 2-近十五分钟， 3-近半小时 ，4-近一小时，5-今天， 6-昨天，7-七天前， 8-当前月，9-上个月
        if (timeLabel == 1){
            startTime = moment().subtract(5, 'minute').format('YYYY-MM-DD HH:mm:ss');
            endTime = moment().format('YYYY-MM-DD HH:mm:ss');
        }else if (timeLabel == 2){
            startTime = moment().subtract(15, 'minute').format('YYYY-MM-DD HH:mm:ss');
            endTime = moment().format('YYYY-MM-DD HH:mm:ss');
        }else if (timeLabel == 3){
            startTime = moment().subtract(30, 'minute').format('YYYY-MM-DD HH:mm:ss');
            endTime = moment().format('YYYY-MM-DD HH:mm:ss');
        }else if (timeLabel == 4){
            startTime = moment().subtract(1, 'hour').format('YYYY-MM-DD HH:mm:ss');
            endTime = moment().format('YYYY-MM-DD HH:mm:ss');
        }else if (timeLabel == 5){
            startTime = moment().startOf('day').format('YYYY-MM-DD HH:mm:ss');
            endTime = moment().endOf('day').format('YYYY-MM-DD HH:mm:ss');
        }else if (timeLabel == 6){
            startTime = moment().subtract(1, 'days').startOf('day').format('YYYY-MM-DD HH:mm:ss');
            endTime = moment().subtract(1, 'days').endOf('day').format('YYYY-MM-DD HH:mm:ss');
        }else if (timeLabel == 7){
            startTime = moment().subtract(6, 'days').format('YYYY-MM-DD HH:mm:ss');
            endTime = moment().endOf('month').format('YYYY-MM-DD HH:mm:ss');
        }else if (timeLabel == 8){
            startTime = moment().startOf('month').format('YYYY-MM-DD HH:mm:ss');
            endTime = moment().format('YYYY-MM-DD HH:mm:ss');
        }else if (timeLabel == 9){
            startTime = moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD HH:mm:ss');
            endTime = moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD HH:mm:ss');
        }

        var appName = $('#app-name-select').val();
        var env = $("#env-select").val();
        var traceId = $("#trace-id").val();
        var logLevel = $("#log-level").val();
        var content = $("#content-input").val();
        var query = {
            "appNameList" : appName,
            "env" : env,
            "startTime" : startTime,
            "endTime" : endTime,
            "traceId" : traceId,
            "level" : logLevel,
            "content" : content,
            "pageNum" : this.pageNumber,
            "pageSize": this.pageSize,
        };
        console.log(JSON.stringify(query));
        return query;
    };
    return oTableInit;
};


function operateFormatter(value, row, index) {//赋予的参数
    return [
        '<a class="btn active disabled" href="#">编辑</a>',
        '<a class="btn active" href="#">档案</a>',
        '<a class="btn btn-default" href="#">记录</a>',
        '<a class="btn active" href="#">准入</a>'
    ].join('');
}