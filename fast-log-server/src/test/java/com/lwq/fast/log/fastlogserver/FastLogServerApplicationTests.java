package com.lwq.fast.log.fastlogserver;

import com.alibaba.fastjson.JSON;
import com.lwq.fast.log.fastlogcore.entity.Message;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
class FastLogServerApplicationTests {


    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Test
    void contextLoads() {

        try {
            GetRequest request = new GetRequest("spring-boot", "11221");
            request.fetchSourceContext(new FetchSourceContext(false));
            // 禁用获取存储字段
            request.storedFields("_none_");
            boolean exists = restHighLevelClient.exists(request, RequestOptions.DEFAULT);
            System.out.println(exists);


        }catch (Exception e){
            e.printStackTrace();
        }






    }

}
