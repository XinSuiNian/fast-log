package com.lwq.fast.log.trace.annotition;

import java.lang.annotation.*;

/**
 *自定义注解实现链路生成
 *
 * @author  刘文强
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface EnableTrace {

}
