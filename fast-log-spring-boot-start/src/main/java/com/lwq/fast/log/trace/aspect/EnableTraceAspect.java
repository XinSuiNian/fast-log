package com.lwq.fast.log.trace.aspect;

import cn.hutool.core.util.IdUtil;
import com.lwq.fast.log.fastlogcore.trace.TraceThreadLocal;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class EnableTraceAspect {

    @Pointcut("@annotation(com.lwq.fast.log.trace.annotition.EnableTrace)")
    public void enableTrace(){}

    @Before("enableTrace()")
    public void doBefore(){
        TraceThreadLocal.set(IdUtil.simpleUUID());
    }

}
