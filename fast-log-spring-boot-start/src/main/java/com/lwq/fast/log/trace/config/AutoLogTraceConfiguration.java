package com.lwq.fast.log.trace.config;


import com.lwq.fast.log.trace.filter.LogTraceFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

/**
 * @author 刘文强
 */
@Configuration
public class AutoLogTraceConfiguration {

    @Bean
    public FilterRegistrationBean logTraceFilterBean(){
        FilterRegistrationBean filterRegistration = new FilterRegistrationBean();
        filterRegistration.setFilter(new LogTraceFilter());
        filterRegistration.addUrlPatterns("/*");
        filterRegistration.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return filterRegistration;
    }
}
