package com.lwq.fast.log.trace.config;

import cn.hutool.core.util.ObjectUtil;
import com.lwq.fast.log.fastlogcore.client.rocketmq.RocketMqClient;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

/**
 * @author 刘文强
 */
@Component
public class DestroyConfig implements DisposableBean {
    @Override
    public void destroy() throws Exception {
        RocketMqClient instance = RocketMqClient.getInstanceForClose();
        if (ObjectUtil.isNotNull(instance)){
            DefaultMQProducer producer = instance.getProducer();
            if (ObjectUtil.isNotNull(producer)){
                producer.shutdown();
            }
        }
    }
}
