package com.lwq.fast.log.trace.interceptor;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.lwq.fast.log.fastlogcore.constant.Constants;
import com.lwq.fast.log.fastlogcore.trace.TraceThreadLocal;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * @author 刘文强
 *
 * 支持feign调用时，trace放入header
 */
@Configuration
@Order(1)
public class TraceFeignInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        String traceId = TraceThreadLocal.get();
        if (StrUtil.isBlank(traceId)){
            traceId = IdUtil.simpleUUID();
        }
        requestTemplate.header(Constants.TRACE_ID,traceId);
    }
}
